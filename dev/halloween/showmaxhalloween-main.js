var ratio = 1,
    particles = [],
    stage,
    canvas, container, screenArea, cloudArea = 384 * 384,
    smokeArea, particleTick, assetsLoaded = false;
var display, soundLoaded = false,
    soundInit, soundTimeout = false,
    soundTriggered = false,
    pageWidth = 0,
    started = false;
var soundApp = soundApp || {};
var checkInt;
var enableCookie = false;
var siteUrl = 'http://localhost/localhost_repos/showmax-halloween/';
var siteUrl = 'http://apps.aquaonline.com/Showmaxhalloween/';
var siteUrl = 'http://krakenstage8.aquawin.com/showmax-halloween/';
function loadSounds(event) {
    if (!soundLoaded && !soundTriggered) {
        soundInit = new soundApp.soundInit();
    }
}
(function() {
    function soundInit() {
        this.init();
    }
    soundInit.prototype = {
        displayMessage: null,
        init: function() {
            if (!createjs.Sound.initializeDefaultPlugins()) {
                return;
            }
            var audioPath = siteUrl + "audio/";
            var sounds = [{
                id: "ambient-01",
                src: "ambient-01.ogg"
            }, {
                id: "break-01",
                src: "break-02.ogg"
            }, {
                id: "build-01",
                src: "build-01.ogg"
            }, {
                id: "grudge-voice-01",
                src: "grudge-voice-01.ogg"
            }, {
                id: "tv-freq-01",
                src: "tv-freq-01.ogg"
            }, {
                id: "tv-freq-02",
                src: "tv-freq-02.ogg"
            }, {
                id: "tv-freq-03",
                src: "tv-freq-03.ogg"
            }];
            var loadProxy = createjs.proxy(this.handleLoad, this);
            var completedProxy = createjs.proxy(this.handleComplete, this);
            var queue = new createjs.LoadQueue(true, audioPath);
            createjs.Sound.alternateExtensions = ["mp3"];
            queue.installPlugin(createjs.Sound);
            queue.addEventListener("fileload", loadProxy);
            queue.addEventListener("complete", completedProxy);
            queue.loadManifest(sounds);
        },
        handleLoad: function(event) {
            soundTriggered = true;
            console.log('file loaded');
        },
        handleComplete: function(event) {
            soundLoaded = true;
            soundTriggered = true;
            //alert('soundLoaded');
            readyCheck();
            //alert('sound loaded');
            //this.playSound('grudge-voice-01');
        },
        playSound: function(soundId, args) {
            var args = args || {};
            var soundInstance = createjs.Sound.play(soundId, args);
            return soundInstance;
        }
    }
    soundApp.soundInit = soundInit;
}());

function getPathFromUrl(url) {
    return url.split(/[?#]/)[0];
}

function prepareOverlay() {
    var base_url = window.location.href;
    base_url = getPathFromUrl(base_url);
    base_url = base_url.substring(0, base_url.lastIndexOf("/") + 1);
    var path = base_url + 'images/';
    path = siteUrl + 'images/';
    var queue = new createjs.LoadQueue(true, path);
    queue.on("fileload", function(event) {
        var image = event.result;
        if (event.item.id == 'bg') {
            setTimeout(function() {
                ratio = image.width / image.height;
                resizeEvent();
            }, 500)

        }
    });
    queue.on("complete", function() {
        assetsLoaded = true;
    });
    var size = '-1024';
    if (pageWidth <= 1024) {
        size = '-1024';
    }
    if (size <= 768) {
        size = '-768';
    }
    images = [{
        id: 'bg',
        src: 'bg' + size + '.jpg'
    }, {
        id: 'glitch1',
        src: 'glitchworx-test' + size + '.gif'
    }, {
        id: 'glitch2',
        src: 'glitchworx-test-02' + size + '.gif'
    }];
    queue.loadManifest(images);
}
var tempAudio;

function startGlitch() {
    var firstDelay = 2000;
    var secondDelay = 4000;
    var thirdDelay = 2000;
    var fourthDelay = 2000;
    setTimeout(function() {
        $('#glitch1').show();
        soundInit.playSound('tv-freq-02');
        setTimeout(function() {
            $('#glitch1').hide();
        }, 500)
        setTimeout(function() {
            $('#glitch2').show();
            soundInit.playSound('tv-freq-03');
            //tempAudio = soundInit.playSound('build-01');
            setTimeout(function() {
                $('#glitch2').hide();
            }, 700)
            setTimeout(function() {
                $('#glitch1').show();
                soundInit.playSound('tv-freq-02');
                setTimeout(function() {
                    $('#glitch1').hide();
                }, 200)
                setTimeout(function() {
                    $('#glitch2').addClass('black-bg').show();
                    afterGlitches();
                    setTimeout(function() {
                        //$('#glitch2').hide();
                    }, 2000)
                }, fourthDelay);
            }, thirdDelay);
        }, secondDelay);
    }, firstDelay);
}

function afterGlitches() {
    var freq = soundInit.playSound('tv-freq-01', {
        loop: 2
    });
    var freq2 = soundInit.playSound('tv-freq-03', {
        loop: 2
    });
    setTimeout(function() {
        soundInit.playSound('grudge-voice-01', {
            loop: 2
        });
        soundInit.playSound('ambient-01', {
            loop: 2
        });
        soundInit.playSound('break-01');
        freq.stop();
        freq2.stop();
    }, 1900)
    setTimeout(function() {
        //tempAudio.stop();
        $('#halloween-overlay').show();
        if(enableCookie){
            $.cookie('showmax-cookie', 'true', {
                path: '/'
            });
        }
        updateOverlay();
        canvas = $('#halloween-canvas')[0];
        stage = new createjs.Stage(canvas);
        container = new createjs.Container();
        container.regX = container.regY = 0;
        stage.addChild(container);
        resizeCanvas();
        setTimeout(function(){
            var player = videojs('my-video');
            $('#video-overlay').fadeIn(500, function(){
                player.play();
                player.on('ended', function() {
                    updateOverlay();
                    $('#halloween-canvas-container').show().css({opacity:1});
                    $('#close-overlay').show();
                    $('#halloween-overlay').removeClass('hide-cursor');
                    updateOverlay();
                    $('#video-overlay').fadeOut(500);
                    setTimeout(function(){
                        updateOverlay();
                    }, 50);
                    $('#films, #happy-halloween-mobile, #films-tablet').hide().css({
                        opacity: 1
                    }).delay(1600).fadeIn(2000, function() {
                        startSmoke();
                    });
                  });
            });
        },4000);

    }, 2000);
}

function resizeEvent() {
    pageWidth = $(window).width();
    var windowWidth = $(window).width();
    var windowHeight = $(window).height();
    var windowRatio = windowWidth / windowHeight;
    if (windowRatio < ratio) {
        $('body').addClass('smaller-ratio');
    } else {
        $('body').removeClass('smaller-ratio');
    }
}

function updateOverlay() {
    $('#halloween-canvas-container').css({
        'margin-top': -$('#halloween-canvas-container').height() / 2
    })
    $('#halloween-canvas-container').css({
        'margin-left': -$('#halloween-canvas-container').width() / 2
    })
}

function updateCanvas() {
    stage.update();
}

function resizeCanvas() {
    var originalWidth = 1909;
    var originalHeight = 1052;
    var currentWidth = $(canvas).parent().width();
    var currentHeight = $(canvas).parent().height();
    var xScale = currentWidth / originalWidth;
    var yScale = currentHeight / originalHeight;
    canvas.width = currentWidth;
    canvas.height = currentHeight;
    container.scaleX = container.scaleY = yScale;
    container.setBounds(0, 0, currentWidth, currentHeight);
    screenArea = currentWidth * currentHeight;
    smokeArea = screenArea / cloudArea;
}
var maxParticles = 100,
    machineEye,
    machineEyeGlow,
    machine,
    container,
    particleSize = 3,
    emissionRate = 1,
    smokeTurbulence = 200,
    smokeTurbulenceAmount = 0,
    startTime,
    particles = [],
    currentTime,
    overlayObjShape,
    ballFriction = 0.99,
    collisionDamper = 0.1,
    lastPos,
    stage,
    objectSize = 3; // drawSize of emitter/field
function startSmoke() {
    resizeCanvas();
    for (var i = 0; i < (smokeArea * 10); i++) {
        newParticle();
    };
    particleTick = setInterval(function() {
        particleTimer();
    }, 50);
}

function updateParticles() {
    for (var i = 0; particles.length > i; i++) {
        particles[i].y += particles[i].velocity.y;
        particles[i].x += particles[i].velocity.x;
        if (particles[i].x > canvas.width + 600 && particles[i].velocity.x > 0) {
            particles[i].hide = true;
            if (particles[i].alpha > 0) {
                particles[i].alpha -= 0.02;
            } else {
                particles[i].x = -400;
                particles[i].y = (Math.random() * (canvas.height + 500)) - 250;
                particles[i].alpha = 0;
                particles[i].hide = false;
            }
        } else if (particles[i].x < -600 && particles[i].velocity.x < 0) {
            particles[i].hide = true;
            if (particles[i].alpha > 0) {
                particles[i].alpha -= 0.005;
            } else {
                particles[i].x = canvas.width + 600;
                particles[i].alpha = 0;
                particles[i].y = (Math.random() * (canvas.height + 500)) - 250;
                particles[i].hide = false;
            }
        }
        if (particles[i].alpha < 0.3 && !particles[i].hide) {
            particles[i].alpha += 0.001;
        } else if (!particles[i].hide) {
            particles[i].alpha = 0.3;
        }
    }
    updateCanvas();
}

function newParticle() {
    var particle = new createjs.Bitmap(siteUrl + "images/Smoke10.png");
    var particleX = (Math.random() * (canvas.width + 500)) - 250;
    var particleY = (Math.random() * (canvas.height + 500)) - 250;
    var particleWidth = 256;
    var particleHeight = 256;
    particle.setBounds(0, 0, particleWidth, particleHeight);
    particle.alpha = 0;
    particle.hide = false;
    particle.scaleX = particle.scaleY = 1.5;
    particle.regX = particle.regY = particleWidth / 2;
    particle.velocity = {
        x: (Math.random() * 5) - 2.5,
        y: 0
    };
    particle.acceleration = {
        x: 0,
        y: 0
    };
    particle.x = particleX;
    particle.y = particleY;
    container.addChild(particle);
    particles.push(particle);
}

function particleTimer() {
    if (particles.length < maxParticles && particles.length < (smokeArea * 20)) {
        newParticle();
    }
    updateParticles();
}

function readyCheck() {
    if (assetsLoaded && !started) {
        if (soundLoaded || soundTimeout) {
            clearTimeout(checkInt);
            started = true;
            startGlitch();
        }
    }
}

function soundCountdown() {
    setTimeout(function() {
        soundTimeout = true;
    }, 2000)
}

function removeCookie() {
    if(enableCookie){
        $.removeCookie('showmax-cookie', {
            path: '/'
        });
    }
}

function justDoIt() {
    $('#close-overlay').after($('<canvas id="halloween-canvas" width="800" height="600"></canvas>'));
    $("#halloween-wrapper").appendTo("body");
    prepareOverlay();
    resizeEvent();
    pageWidth = $(window).width();
    var resizeTimeout;
    $(window).resize(function(event) {
        clearTimeout(resizeTimeout);
        resizeTimeout = setTimeout(resizeEvent, 100);
        resizeEvent();
        updateOverlay();
    });
    loadSounds();
    $('body').on('click', function(e) {
        if (!soundTriggered) {
            soundTriggered = true;
            loadSounds(e);
        }
    });
    $('body').on('mousedown', function(e) {
        if (!soundTriggered) {
            soundTriggered = true;
            loadSounds(e);
        }
    });
    $('body').on('touchstart', function(e) {
        if (!soundTriggered) {
            soundTriggered = true;
            loadSounds(e);
        }
    });
    $('#close-overlay').on('click', function(e) {
        e.preventDefault();
        $('.glitch').hide();
        $('#halloween-overlay').fadeOut(500, function() {
            clearInterval(particleTick);
            createjs.Sound.stop();
            window.location = window.location;
            window.location.reload();
        });
    });
    checkInt = setInterval(function() {
        readyCheck();
    }, 500)
}
$(document).ready(function() {
    if(enableCookie){
        var cookie = $.cookie('showmax-cookie');
    }
    if (typeof cookie == 'undefined') {
        justDoIt();
    }
});
