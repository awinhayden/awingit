var ParticleSystem;
function extendPs(newPs) {
    // Extended
    newPs.fn.particles.updateFunctions.moveTo = {
        enabled: false,
        start: function() {
            newPs.fn.particles.updateFunctions.moveTo.events.started();
            for (var i = 0; i < newPs.particles.length; i++) {
                var _p = newPs.particles[i];
                _p.nextPosition = {
                    x: newPs.canvas.width * Math.random(),
                    y: newPs.canvas.height * Math.random()
                };
                _p.prevPosition = {
                    x: _p.position.x,
                    y: _p.position.y
                };
                _p.moveTo = true;
                _p.duration = 1000 * Math.random() + 1500;
                _p.timestamp = Date.now();
            }
        },
        verticalLine: function(){
        	newPs.fn.particles.updateFunctions.moveTo.events.started();
            for (var i = 0; i < newPs.particles.length; i++) {
                var _p = newPs.particles[i];
                var offset = 0;
                var dist = (newPs.canvas.height / (newPs.particles.length - 1));
                if(i % 2 === 0){
                	offset = dist;
                }
                _p.nextPosition = {
                    x: (newPs.canvas.width / 2) + offset,
                    y: dist * i
                };
                _p.prevPosition = {
                    x: _p.position.x,
                    y: _p.position.y
                };
                _p.moveTo = true;
                _p.duration = 1000 * Math.random() + 1500;
                _p.timestamp = Date.now();
            }
        },
        horizontalLine: function(){
        	newPs.fn.particles.updateFunctions.moveTo.events.started();
            for (var i = 0; i < newPs.particles.length; i++) {
                var _p = newPs.particles[i];
                var offset = 0;
                var dist = (newPs.canvas.width / (newPs.particles.length - 1));
                if(i % 2 === 0){
                	offset = dist;
                }
                _p.nextPosition = {
                    x: dist * i,
                    y: (newPs.canvas.height / 2) + offset
                };
                _p.prevPosition = {
                    x: _p.position.x,
                    y: _p.position.y
                };
                _p.moveTo = true;
                _p.duration = 1000 * Math.random() + 1500;
                _p.timestamp = Date.now();
            }
        },
        events: {
            started: function() {
                newPs.fn.particles.updateFunctions.moveTo.enabled = true;
                newPs.fn.particles.updateFunctions.vectorMove.enabled = false;
            },
            completed: function() {
                //newPs.fn.particles.updateFunctions.vectorMove.enabled = true;
                newPs.fn.particles.updateFunctions.moveTo.enabled = false;
            }
        },
        tools: {
            checkMove: function() {
                var _move = false;
                for (var i = 0; i < newPs.particles.length; i++) {
                    var _p = newPs.particles[i];
                    if (_p.moveTo) {
                        _move = true;
                    }
                }
                return _move;
            }
        },
        call: function(p) {
            var currentTime = Date.now() - p.timestamp;
            var totalTime = p.duration;

            var startValueX = p.prevPosition.x;
            var changeInValueX = p.nextPosition.x - p.prevPosition.x;

            var startValueY = p.prevPosition.y;
            var changeInValueY = p.nextPosition.y - p.prevPosition.y;

            if (currentTime < totalTime) {
                p.position.x = easing.easeInOutCubic(0, currentTime, startValueX, changeInValueX, totalTime);
                p.position.y = easing.easeInOutCubic(0, currentTime, startValueY, changeInValueY, totalTime);
            } else {
                p.moveTo = false;
            }
            if (!newPs.fn.particles.updateFunctions.moveTo.tools.checkMove()) {
                newPs.fn.particles.updateFunctions.moveTo.events.completed();
            }
        }
    }
    return newPs;

}

$(document).ready(function() {
    ParticleSystem = extendPs(new particleJS());
});
